package polymorphism;

import java.util.Calendar;

public class Main {

    public static void main(String[] args) {
//        Manager[] managers = new Manager[10];
//        Employee[] employees = managers;

//        managers[0] = new Employee("Krzysiek", 2000, 2019, Calendar.DECEMBER, 14);

//        Manager manager = new Manager("Krzysiek", 2000, 2019, Calendar.DECEMBER, 14);
//        Employee employee = manager;

        Employee employee = new Employee("Krzysiek", 2000, 2019, Calendar.DECEMBER, 14);
        System.out.println("Moja metoda od Employee");
        employee.mojaMetoda();
        System.out.println("");

        Manager manager = new Manager("Krzysiek", 2000, 2019, Calendar.DECEMBER, 14);
        System.out.println("'Moja metoda od Manager");
        manager.mojaMetoda();

        Employee employee1 = new Manager("Krzysiek", 2000, 2019, Calendar.DECEMBER, 14);
        System.out.println("'Moja metoda od Manager2");
        employee1.mojaMetoda();
        System.out.println(((Manager) employee1).getBonus());
    }
}
