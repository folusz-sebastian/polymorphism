package polymorphism;

public class Manager extends Employee {
    private double bonus;
    public Manager(String n, double s, int year, int month, int day) {
        super(n, s, year, month, day);
        this.bonus = 0;
    }

    public double getBonus() {
        double baseSalary = super.getSalary();
        return baseSalary + bonus;
    }


//    public double getBonus() {
//        return bonus;
//    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public void mojaMetoda() {
        System.out.println("Jestem w Manager");
    }
}
